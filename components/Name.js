import React from 'react'
import Head from 'next/head'
import '../style/styles.css'
const Name = (props)=>{

  const {name, age, home} = props;
  return <div>
    <Head>
      <title>Name</title>
    
    </Head>
<h1 className="name">My name is {name} <br></br> My age is {age}</h1>
  </div>
}
export default Name