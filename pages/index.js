import React from 'react'
import Name from '../components/Name'
import Clock from '../components/Clock'
import Head from 'next/head'

const Index = ()=>{
  return <div>
    <Head>
      <link href="https://fonts.googleapis.com/css?family=Orbitron:700&display=swap" rel="stylesheet" />
    </Head>
    <Clock />
    <Clock />
  </div>
}

export default Index